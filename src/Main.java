import sim.SimStepClasses.Euler;
import sim.SimStepClasses.RungeKutty;
import sim.core.Manager;

public class Main {
    private static final double START_SIMULATION_TIME = 0.0;
    private static final double END_SIMULATION_TIME = 5.0;
    private static final double TIME_STEP = 0.1;

    public static void main(String[] args) {
        Manager manager = Manager.getInstance(START_SIMULATION_TIME, TIME_STEP);
        manager.setEndSimTime(END_SIMULATION_TIME);

//        function: y - x^2
        double x0 = 1;
        new Euler(
                manager,
                (y,x) -> y - x*x,
                x -> 2 + 2*x + x*x - Math.pow(Math.E, x),
                x0,
                "f1"
        );
        new RungeKutty(
                manager,
                (y,x) -> y - x*x,
                x -> 2 + 2*x + x*x - Math.pow(Math.E, x),
                x0,
                "f1"
        );

//        function: y-x^3+6x-5
//        y = x^3+3x^2+5
        x0 = 5;
        new Euler(
                manager,
                (y,x) -> y-Math.pow(x,3)+6*x-5 ,
                x -> Math.pow(x,3)+3*Math.pow(x,2)+5,
                x0,
                "f2"
        );
        new RungeKutty(
                manager,
                (y,x) -> y-Math.pow(x,3)+6*x-5,
                x -> Math.pow(x,3)+3*Math.pow(x,2)+5,
                x0,
                "f2"
        );
        manager.startSimulation();
    }
}
