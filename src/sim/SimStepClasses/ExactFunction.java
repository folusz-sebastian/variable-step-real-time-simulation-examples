package sim.SimStepClasses;

public interface ExactFunction {
    double count(double x);
}
