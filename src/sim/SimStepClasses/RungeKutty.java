package sim.SimStepClasses;

import sim.core.Manager;
import sim.core.SimStep;

public class RungeKutty extends SimStep {
    private final Function function;
    private final ExactFunction exactFunction;
    private double x;
    private final double timeStep;
    String functionName;

    public RungeKutty(Manager mngr, Function function, ExactFunction exactFunction, double x0, String functionName) {
        super(mngr);
        this.function = function;
        this.exactFunction = exactFunction;
        this.timeStep = mngr.getTimeStep();
        this.x = x0;
        this.functionName = functionName;
    }

    @Override
    public void stateChange() {
        double k1 = function.count(x, simTime());
        double k2 = function.count(x + k1*timeStep/2, simTime() + timeStep/2);
        double k3 = function.count(x + k2*timeStep/2, simTime() + timeStep/2);
        double k4 = function.count(x + k3*timeStep, simTime() + timeStep);

//        System.out.println("k: " + k1 + ", " + k2 + ", " + k3 + ", " + k4);

        System.out.println(functionName + ".Runge-Kutty --> x: " + x);

        double exactX = exactFunction.count(simTime());
        System.out.println(functionName + ".Runge-Kutty --> exact x: " + exactX);
        System.out.println(functionName + ".Runge-Kutty --> blad: " + Math.abs(exactX - x));
        System.out.println();

        x = x + timeStep * (k1 + 2*k2 + 2*k3 + k4) / 6;
    }
}
