package sim.SimStepClasses;

import sim.core.Manager;
import sim.core.SimStep;

public class Euler extends SimStep{
    private final Function function;
    private final ExactFunction exactFunction;
    private double x;
    private final double timeStep;
    String functionName;

    public Euler(Manager mngr, Function function, ExactFunction exactFunction, double x0, String functionName) {
        super(mngr);
        this.function = function;
        this.timeStep = mngr.getTimeStep();
        this.exactFunction = exactFunction;
        this.x = x0;
        this.functionName = functionName;
    }

    @Override
    public void stateChange() {
        System.out.println(functionName + ".Euler --> x: " + x);
        double exactX = exactFunction.count(simTime());
        System.out.println(functionName + ".Euler --> exact x: " + exactX);
        System.out.println(functionName + ".Euler --> blad: " + Math.abs(exactX - x));
        x = x + timeStep * function.count(x, simTime());
    }
}
