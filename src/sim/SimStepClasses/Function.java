package sim.SimStepClasses;

public interface Function {
    double count(double y, double x);
}
