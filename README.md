# Variable step real-time simulation examples

##  Opis projektu
Projekt zawiera implementację algorytmów dla symulacji zmiennej w stanie i stałej w czasie. Projekt obejmuje algorytmy: Eulera, Runge-Kutty.
(ang. _Variable step euler method for real-time simulation and Variable step Runge-Kutty method for real-time simulation_)

## Polecenie
Zaimplementuj przykładowe programy symulacyjne wykorzystujące metodę Eulera oraz Runge-Kutty. Porównaj wyniki rozwiązania tego samego problemu przez obie metody